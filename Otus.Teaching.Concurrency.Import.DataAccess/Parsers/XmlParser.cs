﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        private readonly string _fileName;
        private CountdownEvent _countdownEvent;
        public XmlParser(string fileName)
        {
            _fileName = fileName;
        }
        public List<Customer> Parse()
        {
            var customers = new List<Customer>();
            using (var fs = new FileStream(_fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (var reader = new StreamReader(fs))
                {
                    //var customerList = (CustomersList)new XmlSerializer(typeof(CustomersList)).Deserialize(fs);
                    var customerV = (Customer)new XmlSerializer(typeof(Customer)).Deserialize(fs);
                    //customers = customerList.Customers;
                    customers.Add(customerV);
                }
            }
            return customers;
        }

        public List<Customer> ParseString()
        {
            var customers = new List<Customer>();

            using (StreamReader str = new StreamReader(_fileName, Encoding.Default))
            {

                const int threadsCount = 8;

                var lineCount = 0;
                while ((_ = str.ReadLine()) != null)
                {
                    lineCount++;
                }

                var recordCount = (lineCount - 5) / 6;
                var chunkLength = (int)Math.Ceiling(recordCount / (double)threadsCount);
                /*

                using (_countdownEvent = new CountdownEvent(threadsCount))
                {
                    for (int i = 0; i < threadsCount; i++)
                    {
                        var customerChunkParameters = new CustomerChunkParameters();
                        customerChunkParameters.stream = str;
                        customerChunkParameters.chunkLength = chunkLength;
                        customerChunkParameters.StartIndex = customerChunkParameters.chunkLength * i;
                        ThreadPool.QueueUserWorkItem(ParseXML, customerChunkParameters);
                    }
                    _countdownEvent.Wait();
                }
                */
                
                for (int i = 3; i < lineCount-2; i += 6)
                {
                    var lines = string.Join("", File.ReadLines(_fileName).Skip(i).Take(6).ToArray());
                    using (TextReader reader = new StringReader(lines))
                    {
                        var customerV = (Customer)new XmlSerializer(typeof(Customer)).Deserialize(reader);
                        customers.Add(customerV);
                    }
                }


                /*

                var Line = "";
                for (int i = 0; i < 6; i++)
                {
                    Line += str.ReadLine();
                }

                using (TextReader reader = new StringReader(Line))
                    {
                        var customerV = (Customer)new XmlSerializer(typeof(Customer)).Deserialize(reader);
                        customers.Add(customerV);
                    }
                }
                */
            }

            return customers;
        }

        private void ParseXML(object obj)
        {
            var customerChunkParameters = (CustomerChunkParameters)obj;
            for (int i = customerChunkParameters.StartIndex; i < customerChunkParameters.StartIndex + customerChunkParameters.chunkLength; i+=6)
            {
                var lines = File.ReadLines(_fileName).Skip(i).Take(6).ToString();
            }
            _countdownEvent.Signal();
        }

        class CustomerChunkParameters
        {
            public StreamReader stream { get; set; }
            public int StartIndex { get; set; }
            public int chunkLength { get; set; }
        }
    }
}