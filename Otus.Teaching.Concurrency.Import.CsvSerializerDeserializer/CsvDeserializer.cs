﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace CSVSerializeDeserialize
{
    class CsvDeserializer<T>
    {
        public static List<T> Deserialize(string str)
        {
            var res = new List<T>();
            var list = str.Split("\r\n");

            var names = list[0].Split(';');

            var myType = typeof(T);

            for (int i = 1; i < list.Length - 1; i++)
            {
                var objectValues = list[i].Split(';');
                var instance = Activator.CreateInstance(myType);
                for (int j = 0; j < names.Length; j++)
                {

                    var field = myType.GetField(names[j]);
                    field.SetValue(instance, ConvertToType(field, objectValues[j]));
                }
                res.Add((T)instance);
            }

            return res;
        }

        private static dynamic ConvertToType(FieldInfo field, string value)
        {
            if (field.FieldType == typeof(int))
                return int.Parse(value);
            if (field.FieldType == typeof(string))
                return value;
            return value;
        }
    }
}
